﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_Project.Interfaces
{
    interface IInformation
    {
        string GetInformation();
    }
}
