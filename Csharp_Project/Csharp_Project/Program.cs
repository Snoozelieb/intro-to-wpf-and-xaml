﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Csharp_Project.Classes;
using Csharp_Project.Interfaces;

namespace Csharp_Project
{
    class Program
    {
        static double numberTwo = 12.34;

        static void Main(string[] args)
        {
            
            BankAccount bankAccount = new BankAccount(1000);
            bankAccount.AddtoBalance(100);
            Console.WriteLine(bankAccount.Balance);

            SimpleMath simpleMath = new SimpleMath();

            Console.WriteLine(Information(simpleMath));
            Console.ReadLine();
        }

        private static string Information(IInformation information)
        {
            return information.GetInformation();
        }
    }
       
    class SimpleMath : IInformation
    {
        public static double Add(double n1, double n2)
        {
            return n1 + n2;
        }

        public static double Add(double [] numbers)
        {
            double result = 0;
            foreach(double d in numbers)
            {
                result = +d;
            }

            return result;
        }
        public string GetInformation()
        {
            return "Class that solves simple math.";
        }
    }
}
