﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp.Model
{
    public class Metric : INotifyPropertyChanged
    {
        private string amount;

        public string Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                OnPropertyChanged("Amount");
            }
        }

        private string unit;

        public string Unit
        {
            get { return unit; }
            set
            {
                unit = value;
                OnPropertyChanged("Unit");
            }
        }

        private string unitType;

        public string UnitType
        {
            get { return unitType; }
            set
            {
                unitType = value;
                OnPropertyChanged("UnitType");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class Temperature : INotifyPropertyChanged
    {
        private Metric metric;

        public Metric Metric
        {
            get { return metric; }
            set
            {
                metric = value;
                OnPropertyChanged("Metric");
            }
        }

        public Metric Temp_Metric { get; internal set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class WeatherAccu : INotifyPropertyChanged
    {

        private DateTime localObservationDateTime;

        public DateTime LocalObservationDateTime        
        {
            get { return localObservationDateTime; }
            set
            {
                localObservationDateTime = value;
                OnPropertyChanged("LocalObservationDateTime");
            }
        }

        private string weatherText;

        public string WeatherText
        {
            get { return weatherText; }
            set
            {
                weatherText = value;
                OnPropertyChanged("WeatherText");

            }
        }

        private int weatherIcon;

        public int WeatherIcon
        {
            get { return weatherIcon; }
            set
            {
                weatherIcon = value;
                OnPropertyChanged("WeatherIcon");

            }
        }

       private Temperature temperature;

        public Temperature Temperature
        {
            get { return temperature; }
            set
            {
                temperature = value;
                OnPropertyChanged("Temprature");
            }
        }

        private string link;

        public string Link
        {
            get { return link; }
            set
            {
                link = value;
                OnPropertyChanged("Link");
            }
        }

        public WeatherAccu()
        {
            if (DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject()))
                weatherText = "Sunny";

            Temperature = new Temperature()
            {
                Metric = new Metric()
                {
                    Amount = "33",
                    Unit = "C",
                    UnitType = "18"
                }

            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
