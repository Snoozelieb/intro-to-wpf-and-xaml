﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WeatherApp.Model;

namespace WeatherApp.ViewModel
{
    public class WeatherAPI
    {
        public const string API_Key = "TNSToABDHHAcvNoitIlAd7XlHAZaQwek";
        
        public const string BASE_URL = "http://dataservice.accuweather.com/currentconditions/v1/{1}?apikey={0} HTTP/1.1";

        public async Task<WeatherAccu> GetWeatherInformation(string cityName = "328328")
        {
            WeatherAccu result = new WeatherAccu();

            string url = string.Format(BASE_URL, API_Key, cityName);

            using (HttpClient client = new HttpClient())
            {
               var response = await client.GetAsync(url);
               string json = await response.Content.ReadAsStringAsync();

                result = JsonConvert.DeserializeObject<WeatherAccu>(json);
            }
            return result;
        }
    }
}

