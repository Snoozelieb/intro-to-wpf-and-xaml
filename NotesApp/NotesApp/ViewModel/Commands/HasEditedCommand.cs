﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using NotesApp.Model;

namespace NotesApp.ViewModel.Commands
{
    public class HasEditedCommand:ICommand
    {
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            Notebook notebook = parameter as Notebook;
            VM.HasRenamed(notebook);
        }

        public event EventHandler CanExecuteChanged;
        public NotesVM VM { get; set; }

        public HasEditedCommand(NotesVM vm)
        {
            VM = vm;
        }
    }
}
